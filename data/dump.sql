create table `user` (
`id` int(5) NOT NULL AUTO_INCREMENT,
`name` varchar(50),
`email` varchar(50),
PRIMARY KEY  (`id`),
KEY `name` (`name`),
KEY `email` (`email`)
) DEFAULT CHARSET=utf8;

insert into `user` values( 1, "My Name", "myexample@example.org" );

