navigator.id.watch({
	onlogin: function(assertion) {
		var xhr = new XMLHttpRequest();
		xhr.open("POST", "/login/persona/verify", true);
		xhr.setRequestHeader("Content-Type", "application/json");
		xhr.addEventListener("loadend", function(e) {
			var data = JSON.parse(this.responseText);
			if (data && data.status === "okay") {
				document.querySelector("#user").textContent = data.name+" <"+data.email+">";
				document.querySelector("#login").disabled = "disabled";
				document.querySelector("#logout").disabled = "";
			}
		}, false);

		xhr.send(JSON.stringify({
			assertion: assertion
		}));
	
	},
	onlogout: function() {
		var xhr = new XMLHttpRequest();
		xhr.open("POST", "/login/persona/logout", true);
			xhr.addEventListener("loadend", function(e) {
			document.querySelector("#user").textContent = "logged out";
			document.querySelector("#login").disabled = "";
			document.querySelector("#logout").disabled = "disabled";
		});
		xhr.send();
	}
});

document.addEventListener("DOMContentLoaded", function() {
	document.querySelector("#login").addEventListener("click", function() {
		navigator.id.request();
	}, false);

	document.querySelector("#logout").addEventListener("click", function() {
		navigator.id.logout();
	}, false);
}, false);

