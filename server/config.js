var fs = require('fs');
 
var config = function() {
	// constructor
}
 
config.prototype.init = function(file, cb) {
	var data = fs.readFileSync(file);
	var json = JSON.parse(data);
		for (o in json) {
			config.prototype[o] = json[o];
		}
	cb(0); // no error
};
 
module.exports = new config();
