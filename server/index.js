var express = require("express");

var mysql = require('mysql');

var config = require('./config.js');

var db_config = {};
var port = 4646;
var website = "http://persona-test";

config.init('./config.json', function(resp) {
	if (resp != 0) {
			console.log('Could not load config file.');
			return;
	}

	db_config = {
	  host: config.db.host,
	  user: config.db.user,
	  password: config.db.password,
	  database: config.db.database
	};
	
	port = config.port;
	website = config.website;
});


var connection;

function handleDisconnect() {
  connection = mysql.createConnection(db_config); // Recreate the connection, since
                                                  // the old one cannot be reused.

  connection.connect(function(err) {              // The server is either down
    if(err) {                                     // or restarting (takes a while sometimes).
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
    }                                     // to avoid a hot loop, and to allow our node script to
  });                                     // process asynchronous requests in the meantime.
                                          // If you're also serving http, display a 503 error.
  connection.on('error', function(err) {
    console.log('db error', err);
    if(err.code === 'PROTOCOL_CONNECTION_LOST') { // Connection to the MySQL server is usually
      handleDisconnect();                         // lost due to either server restart, or a
    } else {                                      // connnection idle timeout (the wait_timeout
       throw err;                                 // server variable configures this)
    }
  });
}

var app = express();

// The bodyParser, cookieParser, and session middlewares are required for express-persona
// If you don"t want to use express" session middle, you could also use `client-sessions`
app.use(express.logger())
   .use(express.static(__dirname))
   .use(express.urlencoded())
   .use(express.json())
   .use(express.cookieParser())
   .use(express.session({
     secret: "mozillapersona"
}));

// This is how you include express-persona in your app
// In your own app you should use `require("express-persona")`
// You *must* specify the audience option
require("express-persona")(app, {
	audience: website,
	verifyResponse: function(err, req, res, email) {
	  
		handleDisconnect();
		
		var sql = 'SELECT id, name from `user` where email=' + connection.escape(email);
		
		connection.query(sql, function(err, results) {
			if ( err ) {
				console.log("error!");
				res.json({ status: "failure", reason: "MySQL error" });
			} else {
				if ( results.length === 0 ) {
					console.log("none!");
					res.json({ status: "failure", reason: "User not registered" });
				} else {
					console.log("some!");
					req.session.authorized = true;
					// We assume only one entry per email. It's not necessary the case, but it could be handled
					res.json({ status: "okay", email: email, name: results[0].name, id: results[0].id });
					return;
				}
			}
		});
	},
	logoutResponse: function(err, req, res) {
		if (req.session.authorized) {
		  req.session.authorized = null;
		}

		res.json({ status: "okay" });
	}
});


// Launch server
app.listen( port ); 


