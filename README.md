# Example client/server Express app using Persona authentication and MySQL

## Data

In this folder we simply store:
* Database dump used as example (dump.sql)
* Apache vhost configuration (persona-test.apache.conf), which allows linking 
client and server side components. Paths must be adapted to the ones in your computer.
Server name must be changed accordinly in server component configuration.

## Client

Client is kept as a Grunt project. Check at the end of Gruntfile.js file for the available commands and parameters

* Example: grunt siteprod --site /var/www/persona-test
  This will copy to /var/www/persona-test a production ready client


## Server

Express project. It can be executed as 'node index.js' (and port 4646 is used by default)
Different configuration parameters can be set in config.json (e.g., for database access)

